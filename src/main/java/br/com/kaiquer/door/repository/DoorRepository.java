package br.com.kaiquer.door.repository;

import br.com.kaiquer.door.models.Door;
import org.springframework.data.repository.CrudRepository;

public interface DoorRepository extends CrudRepository<Door, Long> {
}
