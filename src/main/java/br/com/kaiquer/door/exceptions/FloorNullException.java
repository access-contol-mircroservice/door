package br.com.kaiquer.door.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "O andar não pode ser nulo")
public class FloorNullException extends RuntimeException {
}
