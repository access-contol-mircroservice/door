package br.com.kaiquer.door.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "O número da sala não pode ser nulo")
public class RoomNullException extends RuntimeException {
}
