package br.com.kaiquer.door.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Porta não encotrada")
public class DoorNotFoudException extends RuntimeException {
}
