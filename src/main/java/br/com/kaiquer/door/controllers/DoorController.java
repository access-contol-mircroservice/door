package br.com.kaiquer.door.controllers;

import br.com.kaiquer.door.models.Door;
import br.com.kaiquer.door.models.dto.DoorMapper;
import br.com.kaiquer.door.models.dto.request.DoorCreateRequest;
import br.com.kaiquer.door.models.dto.response.DoorCreateResponse;
import br.com.kaiquer.door.services.DoorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class DoorController {

    @Autowired
    DoorService doorService;

    @Autowired
    DoorMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    private DoorCreateResponse newDoor(@RequestBody DoorCreateRequest doorCreateRequest) {
        Door door = doorService.create(mapper.toDoor(doorCreateRequest));
        return mapper.toDoorCreateResponse(door);
    }

    @GetMapping("/{id}")
    private DoorCreateResponse findById(@PathVariable(value = "id") Long id) {
        Door door = doorService.findById(id);
        return mapper.toDoorCreateResponse(door);
    }

}
