package br.com.kaiquer.door.services;

import br.com.kaiquer.door.exceptions.DoorNotFoudException;
import br.com.kaiquer.door.exceptions.FloorNullException;
import br.com.kaiquer.door.exceptions.RoomNullException;
import br.com.kaiquer.door.models.Door;
import br.com.kaiquer.door.repository.DoorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DoorService {

    @Autowired
    DoorRepository doorRepository;

    public Door create(Door door) {
        if(door.getFloor() == null) {
            throw new FloorNullException();
        }
        if(door.getRoom() == null) {
            throw new RoomNullException();
        }
        return doorRepository.save(door);
    }

    public Door findById(Long doorId) {
        Optional<Door> door = doorRepository.findById(doorId);
        if (!door.isPresent()) {
            throw new DoorNotFoudException();
        }
        return door.get();
    }

}
