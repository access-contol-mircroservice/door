package br.com.kaiquer.door.models.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DoorCreateResponse {
    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "andar")
    private String floor;

    @JsonProperty(value = "sala")
    private String room;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }
}
