package br.com.kaiquer.door.models.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DoorCreateRequest {
    @JsonProperty(value = "andar")
    private String floor;

    @JsonProperty(value = "sala")
    private String room;

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }
}
