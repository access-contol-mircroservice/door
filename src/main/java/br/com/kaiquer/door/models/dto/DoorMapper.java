package br.com.kaiquer.door.models.dto;

import br.com.kaiquer.door.models.Door;
import br.com.kaiquer.door.models.dto.request.DoorCreateRequest;
import br.com.kaiquer.door.models.dto.response.DoorCreateResponse;
import org.springframework.stereotype.Component;

@Component
public class DoorMapper {

    public Door toDoor(DoorCreateRequest doorCreateRequest) {
        Door door = new Door();

        door.setRoom(doorCreateRequest.getRoom());
        door.setFloor(doorCreateRequest.getFloor());

        return door;
    }

    public DoorCreateResponse toDoorCreateResponse(Door door) {
        DoorCreateResponse response = new DoorCreateResponse();

        response.setId(door.getId());
        response.setRoom(door.getRoom());
        response.setFloor(door.getFloor());

        return response;
    }

}
